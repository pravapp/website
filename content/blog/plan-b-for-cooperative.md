---
title: "Plan B for forming the Cooperative"
date: 2023-03-30T13:43:30+05:30
draft: false
author: false
---
Prav app project plans to form a Multi State Cooperative Society in India. The legal requirement to do so is to have at least 50 members each from at least two Indian states. Currently, we have crossed the 50 member mark from Kerala only and trying to meet the requirement from a second state. The state Maharashtra has 19 members as of now and other states have 6 or less. You can help by [becoming a member](/become-a-member) if you are interested in the prav project or by getting your contacts to be members. The distribution of number of members from each state and districts of Kerala is on the same page.

We have set a deadline for gathering required members to register as a Multi State Cooperative Society through a poll in our [codema](https://codema.in) group (people who have shown interest to help plan the project are part of the group). 

If we don't get 50 members from a second state by 15th June 2023, we plan to register it as a [Cooperative Society in Kerala](https://cooperation.kerala.gov.in/coop/wp-content/uploads/kcsact1969/6.pdf) (it will be limited to members from a single district). We need 25 members from one district of Kerala to register for the same. Later, when we reach required numbers for the multi state society, we can transfer ownership to it.

We are doing this because registration will give us a bank account which can used to receive funds from members and donors which will help move the project forward. 
