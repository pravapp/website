---
title: 'About'
date: 2018-02-22T17:01:34+07:00
draft: false
---
### What is Prav? 

Prav App is convenient without vendor lock-in (unlike WhatsApp, Telegram, Signal etc). Except [Quicksy](https://quicksy.im), most apps are only convenient and come with [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in). We are complementing Quicksy with a more sustainable revenue and democratic governance model through a cooperative ownership and control. We are interoperable with Quicksy and happy to see if more people learn about Quicksy from us and join it instead of Prav too.

Check the [Frequently Asked Questions](/faq) about Prav for more details. 

To get updates about the prav app project, please click the button below:

<style>
.button
{
background-color: #ff1493;
}
</style
<div>
</style>
<a href="https://lists.fsci.in/postorius/lists/announcements.prav.app/"><button class="button">Join Announcements Mailing List</button></a>

If you are interested in taking part in the decision-making process of Prav, you can become a member. 

<style>
.button
{
background-color: #ff1493;
}
</style
<div>
</style>
<a href="/become-a-member"><button class="button">Become A Member</button></a>
<br>
<br>
We are offering private beta accounts to people interested in the project as well, please choose the beta tester option for this.
<style>
.button
{
background-color: #ff1493;
}
</style
<div>
</style>
<a href="/beta-release-party"><button class="button">Become A Beta Tester</button></a>
<br>
<br>
You can also help us by volunteering for some tasks.
<br>
<a href="/volunteer"><button class="button">Become A Volunteer</button></a>
<br>
<p><b>Get in touch for any queries or feedback.
<br>Contact Email: prav at fsci dot in</b></p>
You can also contact us via <a href="https://webchat.pirateirc.net/?channels=#prav">Webchat</a> using a web browser.

### Social Contract

We shall adhere to our Social Contract:

1. We will put rights of users above profit.

2. Our products will always be committed to user privacy.

3. Our apps and services will always respect users' freedom (100% Free Software app and service).

4. Our service will be interoperable using a free standard (federated with any [XMPP](https://xmpp.org/about/technology-overview/) service). No one will be forced to use our app or service to talk to users of our service. Our users can switch to any other service provider without losing the ability to talk to their existing contacts (similar to sim cards provided by telecom operators).

5. We will donate, whenever possible, to the projects we rely on.

## Meet our team

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/badri.jpg"  style="height:250px;">
</figure>

<h3>Badri Sunderarajan</h3>

<b>Helped in setting up initial ejabberd server, domain and hosting, as well as designing posters for Prav Private Beta Release.</b>

<p>Hello! My name is Badri. I am a self-taught programmer and interested in all things FOSS! I try to avoid corporate platforms wherever possible, and encourage others to do the same. I am also an avid reader of books, as well as a writer of mainly nonfiction. E-paper is my screen of choice.</p>

<p>I'm a co-founding editor of the <a href="https://www.snipettemag.com/">Snipette magazine</a>, where I also help to run our YunoHost server which runs services like Ghost and Mattermost and some <a href="https://code.snipettemag.com/snipette">custom-developed tools</a>. I also work part-time for the <a href="https://planethool.org/">Planet Hool foundation</a>, where I convinced them to use XMPP as the basis for their card-game app. Other contributions include the <a href="https://sawaliram.org/">Sawaliram Project</a>, and some internal organisational scripts for the Chennai Mathematical Institute (CMI).</p>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/george-jose.jpg" style="height:250px;">
</figure>

<h3>George Jose</h3>

<p>Hi George here. I am a full stack web Dev(LAMP, MEVN, MERN). I initially got hooked onto prav since I was tired of the existing messengers flaws. I was missing from the discussions for some time due to my work schedule, I hope I can contribute something for the community.</p>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/mujeebcpy.jpg" style="height:250px;">
</figure>

<h3>Mujeebcpy</h3>

<b>Helped prav project grow by making a YouTube video in Malayalam.</b>

<p>Hi, My name is Mujeeb Rahman. Me and my friends run a web development company (<a href="http://alphafork.com/">Alpha fork</a>) with the help of FOSS tools. we also part of migration of a news paper organization to completely move to foss. including scribus and kubuntu. i run a youtube channel named as <a href="http://youtube.com/ibcomputing">IBcomputing</a>. I use this channel to promote FOSS and privacy related topics and general tech topics.</p>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/gn.jpeg" style="height:250px;">
</figure>

<h3>Nagarjuna</h3>

<p>Hi, I am Nagarjuna (GN) , a STEM (science, technology, engineering, and mathematics) educationist with roots in biology and philosophy. Seeking and facilitating freedom, truth, and justice in diverse, decentralized, and distributed structures in codified and real life. Former Professor at Gnowledge lab at HBCSE, TIFR.</p>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/nikhil.png" style="height:250px;">
</figure>

<h3>Nikhil</h3>

<p>Hello, My name is Nikhil, I am a economics student at IIFT Delhi.</p>

<p>I also do sysadmin stuff, i like to tweak system, software, hardware.</p>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/praveen.jpeg"  style="height:250px;">
</figure>

<h3><a href="https://social.masto.host/@praveen">Pirate Praveen</a></h3>

<b>First member of prav project. Helped in outreach and supervising the project.Also, guided Ravi for deploying Quicksy server. 
</b>

<p>A skilled labourer in a social emergency campaigning to remake this world as he sees fit (a pirate). For a world where everyone have enough to survive and can do what they really love to do as opposed to what pays them the most.</p>

<p>“The acquisition of wealth is no longer the driving force in our lives. We work to better ourselves and the rest of humanity.”
- Jean Luc Picard, Star Trek First Contact.</p>

<p>A Debian Developer, privacy advocate and a politician. Mentors students to contribute to Free Software.</p>
<br>
<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/pratik.jpg"  style="width:200px;">
</figure>
<br>
<h3>Pratik M.</h3>
<b> Helped in maintaining Prav website.</b>
<p>Namaste! Pratik here 🙂. I'm a smile freak( though not smiling in the pic:), a high school student with many dreams, and of course, a Free Software enthusiast. I don't have technical wisdom, so, I'm trying to help write some docs(and probably Bangla translation), so that, when Prav becomes a hit, I can smile and say to myself that I had a tiny part there 😉</p>

<br>
<br>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/ravi.jpg"  style="height:250px;">
</figure>

<h3><a href="https://ravidwivedi.in">Ravi Dwivedi</a></h3>
<b>Helped in maintaining prav website, social handle, setting up prav server and project outreach activities.</b>
<br>
<br>
<p>Ravi Dwivedi is a <a href="https://www.gnu.org/philosophy/free-sw.html">Free Software</a> activist who promotes and uses Free Software. He is very strong privacy advocate.</p>

<p>He is a very active member of <a href="https://fsci.in">Free Software Community of India</a> and is a part of Debian India community.</p>

<p>He <a href="https://blog.documentfoundation.org/blog/2022/03/19/join-the-indian-libreoffice-community/">started</a> LibreOffice Community in India. </p>

<p>He is M.Math from Indian Statistical Institute, Kolkata and B.Sc. Mathematics from Acharya Narendra Dev College, Delhi.</p>
	
<p>He writes at <a href="https://ravidwivedi.in">ravidwivedi.in</a> and toots on <a rel="me" href="https://toot.io/@ravi">Mastodon</a>.</p>


