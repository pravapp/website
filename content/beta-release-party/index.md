---
title: "Private Beta Release Party"
---
<figure>
<img src="/images/prav-private-beta.png" width="250">
<figcaption>Poster Credits: Badri</figcaption>
</figure>

We have an initial version of Prav app ready. It is still work in progress. We invited people to test and give us feedback during our private beta release party on 28 January 2023 during the event [MiniDebConf 23](https://tn23.mini.debconf.org/). <a href="https://en.wikipedia.org/wiki/Ravikumar_(writer)">D. Ravikumar</a>, Member of Parliament from Viluppuram was chief guest during the prav private beta release party.

Thanks to <a href="https://vglug.org">VGLUG</a> for providing us the venue for the event.

<b>Disclaimer: Beta invite is valid only for one year and there is no guarantee for storage of your data. We will clean the database if required.</b>

Please fill the form below to get the beta invite.

<a href="https://cryptpad.fr/form/#/2/form/view/KGx5AzZhb4j7CU6PgHcpcdc--gl0rTuHiYozts2y9ns/"><button class="button">Beta Invite Form</button></a>

Below are some photos from the private beta release party in Viluppuram.

<img src="/images/prav-beta-launch-1.jpg">
<br>
<img src="/images/prav-beta-launch-2.jpg">
